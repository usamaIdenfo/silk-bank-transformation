'use strict';
const requests = require('requests');
const parseString = require('xml2js').parseString;
const Excel = require('exceljs')
const fs = require('fs');
const json2xls = require('json2xls');
const PDFParser = require("pdf2json");
const node_xj = require("xls-to-json");
const parser = require('simple-excel-to-json');
const dateFormat = require("dateformat");
const csv=require('csvtojson');

const helper = require('./helper');
const FormattedData = require('./models/FormattedData');
const arrCharFollText = ["s/o","S/O","d/o","D/O","C/O","c/o","alias","ALIAS","caste","CASTE","@","(",")"]
const arrCharReplace = ["and/or","AND/OR","Mr ","MR ","Mrs ","MRS ","MRs ","  "]


// let finalData = new FormattedData('Usama Rahman','30','190');
// console.log(finalData);

var controller = {

  about: function (req, res) {

    // let finalData = new FormattedData('Usama Rahman','4210168672047','1991-01-27','5e5c5s5s','Pakistan','jalil ur rahman','123');
    // console.log(finalData);   

    var jsonArr = []    
    // var a = 'HUMA KHAN//HAMEED UDDIN/SHAHNAZ'
    // console.log(helper.split(a,'/'))
  
    /*
       Reading data from excel and converting it to JSON Format
    */
    var data = parser.parseXls2Json('excel-data/silk-bank-data-dec-21.xlsx');
    // var data = parser.parseXls2Json('excel-data/casa-new-data.xlsx');
    data = data[0]
    console.log(data.length);

    // /**
    //  * Remove Duplication in CNIC & Customer Name Column
    //  */  
    data = data.reduce((unique, o) => {
        if(!unique.some(obj => obj.CNIC.replace('-','') === o.CNIC.replace('-','') && obj.CUSTOMER_NAME === o.CUSTOMER_NAME)) {
          unique.push(o);
        }
        return unique;
    },[]);
    console.log(data.length);

    for(let i=0; i< data.length; i++){      
      
      /**
       * 1- Transform Account Tilte for Companies
       */  
      let customerName = helper.replaceValuesColA(arrCharReplace,data[i].CUSTOMER_NAME)
      customerName = helper.removeCharFollText(arrCharFollText,customerName)
      console.log("Customer Name: "+customerName)
      /**
       * 2- Transform DOB
       */ 
      let dob = helper.formatDateVCC(data[i].DATE_OF_BIRTH)
      console.log("DOB: "+dob);
        /**
       * 3- Transform FatherName
       */ 
      let fatherName = ''
      if(!data[i].FATHERS_NAME){
        fatherName = 'X'
      }else{
        fatherName = helper.replaceValuesColA(arrCharReplace,data[i].FATHERS_NAME)
        fatherName = helper.removeCharFollText(arrCharFollText,fatherName)
        fatherName = helper.removeSpecialChar(fatherName).trim()
      }
      console.log("FatherName: "+fatherName);
      /**
       * 4- Transform CNIC
       */ 
      let cnic=''
      let identityType=''
      if(!data[i].CNIC){
        cnic = '0000000000000'
        identityType = 'id'
      }else{
        cnic = helper.removeSpecialChar(data[i].CNIC).trim()
        /**
         * Check CNIC or Passport
         */
        let isAlphabet = helper.alphanumericisLetter(cnic)

        if(isAlphabet){
          identityType = 'passport'
        }else{
          identityType = 'id'
          /**
           * Check Valid CNIC
          */
          if(cnic.length < 13){
            cnic = '0000000000000'
          }

        }

      }
      console.log("CNIC: "+cnic);
      console.log("IdentityType: "+identityType);
      

      let nationality = data[i].NATIONALITY
      let natId =''
      let country = ''
      if(nationality != '#N/A'){
        /**
       * 5- Transform Nationality & Nat ID
       */
        if(!data[i].NATIONALITY){
          nationality = 'PK'
        }else if(data[i].NATIONALITY == 0){
          nationality = 'PK'
        }else {
          nationality = data[i].NATIONALITY
        }

        nationality = helper.replaceCountryCodes(nationality).trim()
        console.log(nationality)
        nationality = helper.split(nationality,'|')
        country = nationality[0]
        natId = nationality[1]

        if(helper.isCompanyAccount(data[i]) == true){
          customerName = helper.removeMS(customerName).replace(/\s+/g, ' ').trim()
          let finalData = new FormattedData(customerName.replace(/\s+/g, ' ').trim(),cnic, dob,
            natId,country,fatherName,helper.number(jsonArr.length+1),identityType);
          
            jsonArr.push(finalData)
        
        }else{
          
          let custArr = helper.split(customerName,'/')
          if(custArr.length > 1){
            
            for(let j=0 ; j < custArr.length; j++){

              let finalData = new FormattedData(custArr[j].replace(/\s+/g, ' ').trim(),cnic, dob,
                natId,country,fatherName,helper.number(jsonArr.length+1),identityType);
              
                jsonArr.push(finalData)

            }         
          
          }else{
            
            let finalData = new FormattedData(customerName.replace(/\s+/g, ' ').trim(),cnic, dob,
              natId,country,fatherName,helper.number(jsonArr.length+1),identityType);
            jsonArr.push(finalData)
          }
            
        }
      }
    }

  
    // console.log("Data Excel: "+JSON.stringify(jsonArr))

    helper.excel(jsonArr);
    res.send("finish")

    // helper.check("hello");
  },

  cnic:function(req, res){

    console.log(helper.formatDateVCC('1991011'))

    // let data = {firstName:"Usama", lastName:"Rahman",address:""}
    //
    // // let  json =  JSON.parse(data)
    //
    // delete data['firstName']
    // console.log("JSON: ",data)
    // let dob = helper.dateNewFormate("")
    // console.log("DOB: "+dob);

    // var json = {"FATHERS_NAME":"0","DATE_OF_BIRTH":"0","CUSTOMER_NAME":"M/s Usama"}

    // // helper.isCompanyAccount(json)
    
    // console.log(helper.isCompanyAccount(json)); //shows '012123'

    // var jsonArr = []
    // var rawData = parser.parseXls2Json('excel-data/MS-Faulty-records-to-be-deleted.xlsx');
    // rawData = rawData[0]
    // console.log("Raw Data: "+rawData.length);
    //
    // let a = ''
    // for(let i=0; i < rawData.length; i++){
    //     // console.log(rawData[i])
    //     a +='{"idenfoId":'+rawData[i].ID+'},'
    // }
    //
    // let query = "db.customers.find({$and:["+a.trim()+"]})"
    // console.log(query)
    // // var cnicData = parser.parseXls2Json('excel-data/CNICs-to-be-deleted.xlsx');
    // // cnicData = cnicData[0]
    // // console.log("CNICs: "+cnicData.length);
    //
    //
    // // for(let i=0; i<cnicData.length; i++){
    //
    // //   for(let j=0; j<rawData.length; j++){
    //
    // //     if(rawData[j].CNIC == cnicData[i].CNIC){
    // //         console.log(rawData[j])
    // //         jsonArr.push(rawData[j])
    // //     }else{
    // //       console.log('not match')
    // //     }
    // //   }
    // // }
    //
    // // helper.excel(jsonArr);
    // res.send("finish")

  },

  testCallBack: function (req, res) {

    helper.dateNewFormate("Jan 01 1900 ");
    // console.log("AlphaNumeric: "+helper.alphanumericisLetter("213908132"));
    console.log(helper.removeSpecialChar("K / A INDUSTRIES"))

    let a = "K / A INDUSTRIES"

    let b = helper.isCompanyAccount()
    console.log("isCompany: "+b)


    // let a = "IMRAN KHAN"
    // let a ="KH TRAVELS PRIVATE LIMITED UMRAH"
    // let a ="J - S ENTERPRISESPVT."
    // let a ="MR. Usama Rahman"
    
    // let customerName = helper.replaceValuesColA(arrCharReplace,a)

    // console.log(customerName)
    
  }
};

module.exports = controller;