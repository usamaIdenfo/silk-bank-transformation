const express = require('express')
const app = express();
const port = process.env.PORT || 5900;
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


const routes = require('./routes');
routes(app);

app.listen(port, function() {
   console.log('Docker Server started on port: ' + port);
});