const dateFormat = require("dateformat");
const fs = require('fs');
const json2xls = require('json2xls');
const parser = require('simple-excel-to-json');

const dateNew = require('date-and-time');


var helper = {
    // alphanumericisLetter(str) {
    //   return str.length === 1 && str.match(/[a-z]/i);
    // },

    alphanumericisLetter(str)
    {    
      if (str.length !== 1 && str.match(/[a-z]/i)) {    
            return true;    
      }    
      return false;   
    },

    dateNewFormate(dateStr){
      // let a = "Jul  8 1961"
        let formatedDate= ''

        try{

            if(dateStr === ''){
                formatedDate = '1900-01-01'
            }else{
                let now = new Date(dateStr);
                formatedDate = dateNew.format(now, 'YYYY-MM-DD')
                console.log("Date: "+formatedDate)
            }

        }catch (err){
            formatedDate = '1900-01-01'
        }

      return formatedDate;
    },

    formatDateVCC:function (dateStr){
        let finalData = ''

        try{

            // dateStr = ''
            if(dateStr && dateStr.length >= 8){

                finalData = this.addStr(dateStr,4,'-')
                finalData = this.addStr(finalData,7,'-')
                // finalData = this.addStr(finalData,5,'')
            }else{
                console.log('less than 8 C')
                finalData = '1900-01-01'

            }
        }catch (err){
            console.log('err',err)

            finalData = '1900-01-01'

        }

        return finalData;

    },

    addStr:function (str, index, stringToAdd){
        return str.substring(0, index) + stringToAdd + str.substring(index, str.length);
    },

    formateDateDirector(dateStr) {

        try {
            let date = ""

            if (dateStr === '') {
                date = '1900-01-01'
            } else {
                date = formatDate(dateStr, "YYYY-MM-DD")
            }
        }catch (err){
            date = '1900-01-01'
        }

        return date
    },

    split:function(str,seperator){
        let string = str.split(seperator);
        return string
    },

    replaceValuesColA:function(arr,str){

        
        str = str.toString()
         /**
            * Removing '-' '.' ','
            */
         if(str.includes('-')){
             str = str.replace(/-/g,' ')

         }
         console.log("Replace1 :"+str)

         if(str.includes('.')){
            //  str = str.replace(/./g,' ')
            str = str.split('.').join("");

         }
         console.log("Replace2 :"+str)

         if(str.includes(',')){
             str = str.replace(/,/g,' ')
         }
         console.log("Replace3 :"+str)
        // str = str.replace(/-/g,' ')
        // str = str.replace(/./g,' ')
        // str = str.replace(/,/g,' ')
       
        for(let i=0; i < arr.length; i++){
            /**
            * Removing special characters except '/'
            */
            str = str.replace(/[^A-Z a-z/]/g,"")
            /**
             * Replacing multiple spaces with single space
             */
            str =str.replace(/  +/g, ' ');
            re1 = new RegExp(arr[i], "g")
            str = str.replace(re1,"")


        }

        console.log("Replace Final :"+str)
        
        return str
    },

    removeCharFollText:function(arr,str){
    
        for(let i=0; i < arr.length; i++){
            str = str.split(arr[i])[0]
        }

        console.log("Char :"+str)

        return str
    },

    removeSpecialChar: function(str){
        str = str.toString()
        let string = str.replace(/[^0-9a-zA-Z ]/g, "")
        return string
    },

    check:function(str){
        console.log('check-helper'+str)
    },

    excel:function(json) {
        var xls = json2xls(json);
        // fs.writeFileSync('CASA-Silk-final-july-2021.xlsx', xls, 'binary');
        fs.writeFileSync('Silk-transformation-DEC-2021.xlsx', xls, 'binary');
    },
    isCompanyAccount:function(json){
        if((json.FATHERS_NAME == 0 && json.DATE_OF_BIRTH == 0) 
            || this.checkMSIn(json.CUSTOMER_NAME) || this.companyContains(json.CUSTOMER_NAME)){
            return true;
        }else{
          return false;
        }
  
    },
    checkMSIn: function(str){
      var strFirstThree = str.substring(0,3);
      if(strFirstThree.toLocaleLowerCase() == "m/s"){
        return true
      }else{
        return false
      }    
    },

    companyContains: function(str){

      let strr = str.toLocaleLowerCase()
      switch (strr) {
          case 'industries ':
              true
              break;
          case 'pvt ltd ':
              true
              break;
          case 'ltd ':
              true
              break;
          case 'engineering ':
              true
              break;        
          case 'travels ':
            true
            break;
          case 'private ltd ':
              true
              break;
          case 'textiles ':
              true
              break;
          case 'company ':
              true
              break;   

          case 'pvt ltd. ':
            true
            break;        
          case 'associates ':
            true
            break;
          case 'foundation ':
              true
              break;
          case 'corporation ltd ':
              true
              break;
          case 'services ':
              true
              break;
          default:
              false
              break;
      }

    },
    formatDate: function(dateStr){
        let formatedDate = '';        
        try {
          if(dateStr == 0){
            formatedDate = '1900-01-01'
          }else{
            formatedDate = dateStr.replace(/(\d{4})(\d{2})(\d{2})/, "$1-$2-$3");
          }
        } catch (error) {
          formatedDate = '1900-01-01';
        }
  
        return formatedDate;
        
    },
    replaceCountryCodes: function(str){
        let codenName = ''
        switch (str) {
            case 'AC':
                str = 'PK'
                break;
            case 'AN':
                str = 'PK'
                break;
            case 'EU':
                str = 'PK'
                break;
            case 'UK':
                str = 'GB'
                break;
            default:
                str = str
                break;
        }
        /**
         * Reading CountryList Mapping
         */
        // var data = parser.parseXls2Json('excel-data/country-list-silk.xlsx');
        // data = data[0]
        // // console.log(data.length);
        // for(let j=0; j < data.length; j++){
        //     console.log(data[j])
        //     if(str == data[j].Code){
        //         codenName = data[j].Country+"|"+data[j].nationalityId
        //         break;
        //     }
        // }

        codenName = str+"|5efeaea522adf8055ac1e7d8"

        return codenName;
    },
    removeMS:function(str){
        var pattern1 = "M/S"
        var pattern2 = "M/s"
        re1 = new RegExp(pattern1, "g")
        re2 = new RegExp(pattern2, "g")
        str = str.replace(re1,"")
        str = str.replace(re2,"")
        str = str.replace(/\//g," ")
        return str.trim();
    },
    number: function(num){
        var returnVal = '';
        if(num.toString().length == 1){
          returnVal = "00000"+num;
        }else if(num.toString().length == 2){
          returnVal = "0000"+num;
        }else if(num.toString().length == 3){
          returnVal = "000"+num;
        }else if(num.toString().length == 4){
          returnVal = "00"+num;
        }else if(num.toString().length == 5){
          returnVal = "0"+num;
        }else {
          returnVal = num;
        }
        return returnVal;
      }
}

module.exports = helper;