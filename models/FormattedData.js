class FormattedData {
    constructor(firstName, lastName, dob,natId,nationality,fatherName,serialNo,identityType) {
        // always initialize all instance properties
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.natId = natId;
        this.nationality = nationality;
        this.fatherName = fatherName;
        this.identityType = identityType
        this.serialNo = serialNo;
    }
    getFirstName() {
        return this.firstName;
    }
    getLastName() {
        return this.lastName;
    }
    getDOB() {
        return this.dob;
    }
    getNatId() {
        return this.natId;
    }
    getNationality() {
        return this.nationality;
    }
    getFatherName() {
        return this.fatherName;
    }
    getSerialNo() {
        return this.serialNo;
    }    
    getIdentityType() {
        return this.identityType;
    }
}

module.exports = FormattedData